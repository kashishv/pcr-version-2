from time import sleep 
import Adafruit_BBIO.PWM as PWM
import Adafruit_BBIO.GPIO as GPIO


''' 
#TEST for checking a GPIO pin
i=0
pin="P8_21"
while i<10:
	GPIO.setup(pin,GPIO.OUT)
	GPIO.output(pin,GPIO.HIGH)
	sleep(1)
	GPIO.output(pin, GPIO.LOW)
	sleep(1)
	i=i+1

'''  
PWM.start("P8_13", 0, 1000)
PWM.stop("P8_13")

GPIO.setup("P9_11", GPIO.OUT)
GPIO.output("P9_11", GPIO.HIGH)
sleep(3)
GPIO.output("P9_11", GPIO.LOW)


GPIO.cleanup()
PWM.cleanup()





