#importing all requirements
import sys
import csv
import math
import os.path
from pidclass import PID
import time
from time import sleep
import Adafruit_MAX31855.MAX31855 as MAX31855
import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.PWM as PWM
import Adafruit_BBIO.ADC as ADC

ADC.setup()

#Asking for the recipe text file
print " "
filename = raw_input("Enter the name of the .txt file with your recipe (format: example.txt): ")
#opening the input file with the recipe and PID parameters
f=open(filename,"r")

#Ask the user to specify the name of the desired output file
print " "
out_file = raw_input("Enter the name of the .csv file you want to output your results to (format: example.csv): ")

#Asking for Duty Cycle
print " "
DutyCycle = int(raw_input("Enter Duty cycle for ramping up/down during thermal cycling (0-100): "))

#When the output.csv file already exists - asking the user to either change the name or overwrite existing file
if os.path.exists(out_file)==True:
        print " "
        print ".csv File with the same name already exists"
        print " "
        nextstep=raw_input("Do you want to continue and overwirte the file (Answer: y/n): ")

        if nextstep=='y' or nextstep =='Y' or nextstep == 'Yes' or nextstep=='yes' or nextstep=='YES':
                pass
        else:
                print " "
                out_file = raw_input("Please re-enter the new name of the .csv file you want to output your results to (format: example.csv): ")


#read the lines from the input file
lines = f.readlines()

#create empty array to store:PID parameters, temperature and time for each step
info = []

#Defining a function that ignores all lines starting with '#'
def skip_comments(f):
        for line in lines:
                if not line.strip().startswith('#'):
                        yield line

#Defining a function that ignores a line starting with a blank or space
def skip_blanks(f):
        for l in f:
                line=l.rstrip()
                if line:
                        yield line

def readTemp(pin):
	potVal=ADC.read_raw(pin)
        Vin = (1.8*potVal)/(4096*13)
        V1 = 1.8 - Vin
        I = V1/2000
        sensorR = Vin/I
        Temp = (2.6123*sensorR) - 261.71
        return Temp
	

#Take the original recipe file and pass through skip_comments function to get a new file
f_1 = skip_comments(f)

#Take modified file from skip_comments and pass through skip_blank function to get a new final file from which parameters are obtained
f_2 = skip_blanks(f_1)

#reading each line inthe text file
for line in f_2:
        #splitting each line into number
        numbers = line.split(',')
        #converting the word into integer and appending it into the array
        for i in numbers:
                info.append(float(i))


#Opening our csv file to write
csvfile = open(out_file, 'wb')

#outputting to the user to confirm if the PID parameters and the recipe is correct

print "P = ", info[0]
print "I = ", info[1]
print "D = ", info[2]


i=3
while i<len(info):
        print "Step",int(info[i]), ": ", info[i+1],"*C for ", info[i+2], " seconds."
        i = i+3
#-----------------------------------------------------------------------

#Assigning pins for temperature sensors for surface and heat sink.
an1 = "P9_39"
an2 = "P9_37"


#Assigning pins for thermocouple for sensing the temperature inside the cartridge.
CLK = 'P9_13'
CS = 'P9_27'
DO = 'P9_30'

sensor = MAX31855.MAX31855(CLK,CS,DO)

#Assigning pin for PWM output
pwmPin = "P8_13"

#Assigning pin for H-bridge
In1 = "P9_11"

#Assigning pin for safety
safety = "P8_17"

#Setting up H-bridge Heat/Cool pins
GPIO.setup(In1, GPIO.OUT)

#Safety up safety pin
GPIO.setup(safety, GPIO.OUT)

#Checking from the user if the recipe is correct
answer = raw_input("Shall we proceed? (Answer: y/n) ")

#Assigning Duty Cycle values
DC1 = DutyCycle
DC2 = DutyCycle

#--------------------------------------------------------------------------

#If the recipe is correct and we want to begin the PCR cycling
if answer=='y' or answer =='Y' or answer == 'Yes' or answer=='yes' or answer=='YES':

        #Setting the PID parameters
        p_parameter = info[0]
        i_parameter = info[1]
        d_parameter = info[2]
        pid=PID(p_parameter,i_parameter,d_parameter)

        #Writing onto the csv file
        csvfile.write("P, I, D\n")
        row=str(p_parameter)+","+str(i_parameter)+","+str(d_parameter)+"\n"
        csvfile.write(row)
        csvfile.write("Begin Logging \n")
        csvfile.write(" \n")
        csvfile.write("Time(s), Surface Temp(*C), Channel Temp(*C), Heat Sink Temp(*C), Set Temp(*C), Duty Cycle(%)\n")
#----------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------
        i = 3
        logtime = time.time()
        while i<len(info):
                GPIO.output(safety, GPIO.LOW)
                setTemp = info[i+1]
                delay=info[i+2]
                pid.setPoint(setTemp)
                #mytemp = sensor.readTempC()
		mytemp = readTemp(an1)
		while math.isnan(mytemp):
                        #print "OH NO"
                        mytemp=readTemp(an1)
                #Heating Mode
                if mytemp < setTemp:
                        #Enabling the pins to heat: 0/LOW is for heating
                        GPIO.output(In1, GPIO.LOW)

			#Setting a starting point for Integrator so the output Duty Cycle from PID is limited (temperature does not drop too low when PID starts)
                        pid.setIntegrator(60)
			
                        while setTemp-mytemp > 7:
                                PWM.start(pwmPin, DC1, 10000)
                                sleep(0.5)
                                mytemp=readTemp(an1)
                                heatsink=readTemp(an2)
                                channel = sensor.readTempC()
                                while math.isnan(mytemp):
                                        #print "OH NO"
                                        mytemp=readTemp(an1)
                                print " "
                                print "T(*C): ",mytemp, "Ch(*C): ",channel, "HS(*C): ", heatsink
                                print "t(s): ", time.time()-logtime
                                print "DC: ", DC1 
                                csvfile.write(str(time.time()-logtime)+","+str(mytemp)+","+str(channel)+","+str(heatsink)+","+","+str(DC1)+"\n")
                        while setTemp-mytemp > 1:
                                PWM.start(pwmPin, DC1, 10000)
                                sleep(0.5)
                                mytemp=readTemp(an1)
                                heatsink=readTemp(an2)
                                channel = sensor.readTempC()
                                while math.isnan(mytemp):
                                        #print "OH NO"
                                        mytemp=readTemp(an1)
                                print " "
                                print "T(*C): ",mytemp, "Ch(*C): ", channel,"HS(*C): ", heatsink
                                print "t(s): ", time.time()-logtime
                                print "DC: ", DC1
                                csvfile.write(str(time.time()-logtime)+","+str(mytemp)+","+str(channel)+","+str(heatsink)+","+","+str(DC1)+"\n")


                        print " "
                        print 'Entering timer/PID mode'
                        PWM.stop(pwmPin)
                        mytemp=readTemp(an1)
                        while math.isnan(mytemp):
                                #print "OH NO"
                                mytemp=readTemp(an1)
                        value = pid.update(mytemp)
                        start = time.time()

                        while time.time()-start <= delay:
                                mytemp=readTemp(an1)
                                heatsink = readTemp(an2)
                                channel = sensor.readTempC()
                                while math.isnan(mytemp):
                                        #print "OH NO"

                                        mytemp=readTemp(an1)
                                value = pid.update(mytemp)
                                #value = value/2
                                #print value
                                DC = (value/setTemp) * 100
                                #print DC
                                if DC<=0:
                                        DC=0
                                elif DC>100:
                                        DC=100

                                #Checking whether to heat or cool H-bridge
                                if mytemp<setTemp:
                                        GPIO.output(In1,GPIO.LOW)
                                elif mytemp>setTemp:
                                        GPIO.output(In1,GPIO.HIGH)


                                PWM.start(pwmPin, 0, 10000)
                                PWM.set_duty_cycle(pwmPin, DC)
                                #PWM.set_duty_cycle(pwmPin, 30)
                                sleep(0.5)
                                print " "
                                print "T(*C): ",mytemp, "Ch(*C): ", channel,"HS(*C): ", heatsink
                                print "t(s): ", time.time()-logtime
                                print "DC: ", DC
                                csvfile.write(str(time.time()-logtime)+","+str(mytemp)+","+str(channel)+","+str(heatsink)+","+str(setTemp)+","+str(DC)+"\n")
                                print " "


                        PWM.stop(pwmPin)





                #Cooling Mode
                elif mytemp>setTemp:
                        #Enabling the pins to cool: 1/HIGH is for cooling
                        GPIO.output(In1, GPIO.HIGH)

			#Setting Integrator so the temperature doesn't drop/rise too much when PID initially starts
			pid.setIntegrator(20)
                        while mytemp-setTemp > 7:
                                PWM.start(pwmPin, DC2, 10000)
                                sleep(0.5)
                                mytemp=readTemp(an1)
                                heatsink = readTemp(an2)
                                channel=sensor.readTempC()
                                while math.isnan(mytemp):
                                        #print "OH NO"
                                        mytemp=readTemp(an1)
                                print " "
                                print "T(*C): ",mytemp, "Ch(*C): ",channel, "HS(*C): ", heatsink
                                print "t(s): ", time.time()-logtime
                                print "DC: ", DC2
                                csvfile.write(str(time.time()-logtime)+","+str(mytemp)+","+str(channel)+","+str(heatsink)+","+","+str(DC2)+"\n")
                        while mytemp-setTemp > 1:
                                PWM.start(pwmPin, DC2, 10000)
                                sleep(0.5)
                                mytemp=readTemp(an1)
                                heatsink = readTemp(an2)
                                channel=sensor.readTempC()
                                while math.isnan(mytemp):
                                        #print "OH NO"
                                        mytemp=readTemp(an1)
                                print " "
                                print "T(*C): ",mytemp, "Ch(*C): ",channel, "HS(*C): ", heatsink
                                print "t(s): ", time.time()-logtime
                                print "DC: ", DC2
                                csvfile.write(str(time.time()-logtime)+","+str(mytemp)+","+str(channel)+","+str(heatsink)+","+","+str(DC2)+"\n")

                        print ' '
                        print 'Entering timer/PID mode'
                        PWM.stop(pwmPin)
                        mytemp=readTemp(an1)
                        while math.isnan(mytemp):
                                #print "OH NO"
                                mytemp=readTemp(an1)
                        value=pid.update(mytemp)
                        start = time.time()

                        #Switching H-bridge current
                        GPIO.output(In1, GPIO.LOW)
                        while time.time()-start <=delay:
                                mytemp=readTemp(an1)
                                heatsink = readTemp(an2)
                                channel=sensor.readTempC()
                                while math.isnan(mytemp):
                                        #print "OH NO"
                                        mytemp=readTempC(an1)
                                value = pid.update(mytemp)

                                #print value
                                DC = (value/setTemp)*100
                                #print DC
                                if DC<=0:
                                        DC=0
                                elif DC>100:
                                        DC=100

                                #Checking to heat or cool - H-bridge
                                if mytemp<setTemp:
                                        GPIO.output(In1,GPIO.LOW)
                                elif mytemp>setTemp:
                                        GPIO.output(In1,GPIO.HIGH)



                                PWM.start(pwmPin, 0, 10000)
                                PWM.set_duty_cycle(pwmPin, DC)
                                #PWM.set_duty_cycle(pwmPin, 30)
                                sleep(0.5)
                                print " "
                                print "T(*C): ",mytemp, "Ch(*C): ", channel,"HS(*C): ", heatsink
                                print "t(s): ", time.time()-logtime
                                print "DC: ", DC
                                csvfile.write(str(time.time()-logtime)+","+str(mytemp)+","+str(channel)+","+str(heatsink)+","+str(setTemp)+","+str(DC)+"\n")
                                print " "


                        PWM.stop(pwmPin)
                GPIO.cleanup()
                PWM.cleanup()
                i = i+3

#If the recipe is not correct and we want to edit the recipe text file
else:

        print " "
        print '    Please either edit your current recipe file or upload a new recipe version'
        print ' '
        print '            To edit: Press "vi sample.txt" on command line and press Enter'
        print '                     Press "i" to start editing'
        print '                     Press Esc, ":wq" and then Enter to exit the file back to command line'
        print ' '
        print '            To upload new version: Please refer to the Operations Manual'



#---------------------------------------------------------------------------------------------------
